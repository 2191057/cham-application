import csv

def mainMenu():
    print("This program will help you find nearest hospitals in your area")
    print("1. Display hospitals list in a specific region.")
    print("2. Search hospital in list. (Search by city)")
    print("3. Quit the program")
    selection = int(input("Enter your choice: "))
    if selection == 1:
        load()
    elif selection == 2:
        search()
    elif selection == 3:
        print("Thank you for using the program.")
        exit()
    else:
        print("Invalid choice, please select from 1 to 3")
        mainMenu()


def load():
    print(
        "Please choose your region\n 1 for Region 1\n 2 for CAR \n 3 for Region II"
        "\n 4 for Region III\n 5 for Region IV\n 6 for NCR\n 7 for Region V\n 8 for Region VI"
        "\n 9 for Region VII\n 10 for SOCCSKARGEN\n 11 for Region VIII\n 12 for CARAGA\n 13 for Region IX"
        "\n 14 for Region X\n 15 for Region XI ")
    option = int(input("Your option: "))

    if option == 1:
        print("Region 1\n")
        csv_file = csv.reader(open('../files/Region I.csv', 'r'))
        for row in csv_file:
            print(row)
        anykey = input("Press any key to return from main menu")
        mainMenu()

    elif option == 2:
        print("CAR\n")
        csv_file = csv.reader(open('../files/CAR.csv', 'r'))
        for row in csv_file:
            print(row)
        anykey = input("Press any key to return from main menu")
        mainMenu()

    elif option == 3:
        print("Region 2\n")
        csv_file = csv.reader(open('../files/Region II.csv', 'r'))
        for row in csv_file:
            print(row)
        anykey = input("Press any key to return from main menu")
        mainMenu()

    elif option == 4:

        print("Region 3\n")
        csv_file = csv.reader(open('../files/Region III.csv', 'r'))
        for row in csv_file:
            print(row)
        anykey = input("Press any key to return from main menu")
        mainMenu()

    elif option == 5:
        print("Region 4\n")
        csv_file = csv.reader(open('../files/Region IV.csv', 'r'))
        for row in csv_file:
            print(row)
        anykey = input("Press any key to return from main menu")
        mainMenu()

    elif option == 6:
        print("NCR 4\n")
        csv_file = csv.reader(open('../files/NCR.csv', 'r'))
        for row in csv_file:
            print(row)
            print()
        anykey = input("Press any key to return from main menu")
        mainMenu()

    elif option == 7:
        print("Region 5\n")
        csv_file = csv.reader(open('../files/Region V.csv', 'r'))
        for row in csv_file:
            print(row)
        anykey = input("Press any key to return from main menu")
        mainMenu()

    elif option == 8:
        print("Region 6\n")
        csv_file = csv.reader(open('../files/Region VI.csv', 'r'))
        for row in csv_file:
            print(row)
        anykey = input("Press any key to return from main menu")
        mainMenu()

    elif option == 9:
        print("Region 7\n")
        csv_file = csv.reader(open('../files/Region VII.csv', 'r'))
        for row in csv_file:
            print(row)
        anykey = input("Press any key to return from main menu")
        mainMenu()

    elif option == 10:
        print("SOCCSKARGEN\n")
        csv_file = csv.reader(open('../files/SOCCSKARGEN.csv', 'r'))
        for row in csv_file:
            print(row)
        anykey = input("Press any key to return from main menu")
        mainMenu()

    elif option == 11:
        print("Region 8\n")
        csv_file = csv.reader(open('../files/Region VIII.csv', 'r'))
        for row in csv_file:
            print(row)
        anykey = input("Press any key to return from main menu")
        mainMenu()

    elif option == 12:
        print("CARAGA\n")
        csv_file = csv.reader(open('../files/CARAGA.csv', 'r'))
        for row in csv_file:
            print(row)
        anykey = input("Press any key to return from main menu")
        mainMenu()

    elif option == 13:
        print("Region 9\n")
        csv_file = csv.reader(open('../files/Region IX.csv', 'r'))
        for row in csv_file:
            print(row)
        anykey = input("Press any key to return from main menu")
        mainMenu()

    elif option == 14:
        print("Region 10\n")
        csv_file = csv.reader(open('../files/Region X.csv', 'r'))
        for row in csv_file:
            print(row)
        anykey = input("Press any key to return from main menu")
        mainMenu()

    elif option == 15:
        print("Region 11\n")
        csv_file = csv.reader(open('../files/Region XI.csv', 'r'))
        for row in csv_file:
            print(row)
        anykey = input("Press any key to return from main menu")
        mainMenu()

def search():
    city = input("Enter your city: ")
    csv_file = csv.reader(open('../files/test.csv', 'r'))

    not_found = True
    for row in csv_file:
        if city.lower() == row[2].lower():
            print(row)
            not_found = False
            
    if not_found:
        print("The city that you are looking is not in the list")
    anykey = input("Press any key to return from main menu")
    mainMenu()

exit()

